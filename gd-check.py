## josh ##
from pywebio import start_server
from pywebio.input import input, FLOAT, input_group, NUMBER, select, TEXT
from pywebio.output import*
from godaddypy import Client, Account
import json
import os
import time
import requests


def DoaminCheck(ApiKey,ApiSec):
    MyAcc = Account(api_key = ApiKey, api_secret = ApiSec)
    client = Client(MyAcc)
    DomainList = client.get_domains(limit=1000)
    DomainNum = len(DomainList)

    #排除列表
    OutList = []
    #域名解析資訊
    Domains = {}
    #Count用於計算域名數量
    Count = 0

    #若域名狀態ACTIVE及nameserver未託管出去Count數量+1
    for Domain in DomainList:
        try:
            DomainInfo = client.get_domain_info(Domain)
            DomainStatus = DomainInfo.get('status')
            DomainNS =DomainInfo.get('nameServers')
            #檢查狀態是否為:ACTIVE,若不是添加到排除列表
            if DomainStatus != 'ACTIVE':
                OutList.append(Domain)

            else:
                CheckNum = 0

                for NameServer in DomainNS:
                    #檢查nameserver是否為godaddy預設,若不是會找不到domaincontrol,值會是-1
                    if NameServer.find('domaincontrol') == -1:
                        CheckNum +=1

                if CheckNum !=0:
                    OutList.append(Domain)
                else:
                    time.sleep(1)
                    #取得域名的record訊息
                    Domains[Domain] = client.get_records(Domain)

        except:
            OutList.append(Domain)

        Count +=1

    for Domain in Domains:
        for RecordInfo in Domains[Domain]:
            if (RecordInfo['data'].find('Parked') == -1 and
                RecordInfo['data'].find('domaincontrol') == -1 and
                RecordInfo['data'].find('_domainconnect') == -1 and
                RecordInfo['data'].find('@') == -1 and
                RecordInfo['name'] != '@' and
                RecordInfo['name'] != '_domainconnect' and
                RecordInfo['name'] != 'www'):
                OutList.append(Domain)

    #可用域名列表 
    OkDomain = []

    #若域名沒有在排除列表就添加到可用列表
    for domain in DomainList:
        if domain not in list(set(OutList)):
            OkDomain.append(Domain)

    for domain in OkDomain:
        put_text(str(domain))
    put_text("域名總數:" + (str(Count)))
    put_text("可用剩餘:" + (str(len(OkDomain))))

def web():
    info = input_group("歡迎使用域名查詢，請輸入key&sec",[
        input('ApiKey', name='ApiKey', value="ApiKey"),
        input('ApiSec', name='ApiSec', value="ApiSec")
    ])
    DoaminCheck(info['ApiKey'],info['ApiSec'])
    
if __name__ == '__main__':
    start_server(web, port=8080)
